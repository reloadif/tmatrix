#pragma once
#include <iostream>
#include <exception>
#include <locale>

#include "myStreamTVector.hpp"

#define MAXSIZE 3;


template < class Vtype >
class TVector {

protected:
	int sizeVArray;
	int positionInMatrix;
	Vtype* VArray;

	friend std::istream& operator >> <Vtype> (std::istream& stream, TVector& obj);
	friend std::ostream& operator << <Vtype> (std::ostream& stream, const TVector& obj);

public:
	TVector();
	explicit TVector(const int _sizeVArray);

	TVector(const int _sizeVArray, const int _positionInMatrix, const Vtype value);
	TVector(const int _sizeVArray, const int _positionInMatrix, const Vtype* _VArray);
	TVector(const TVector& vector);

	~TVector();

	int getSizeVArray() const;
	int getPositionInMatrix() const;

	Vtype vectorLenght() const;


	TVector operator+(const Vtype value);
	TVector operator-(const Vtype value);

	TVector operator+(const TVector& vector);
	TVector operator-(const TVector& vector);

	TVector operator*(const Vtype value);
	Vtype operator*(const TVector& vector);

	TVector& operator=(const TVector& vector);

	const bool operator==(const TVector& vector);
	const bool operator!=(const TVector& vector);


	Vtype& operator[](const int index);
	const Vtype& operator[](const int index) const;
};


template < class Vtype >
TVector<Vtype>::TVector() {
	setlocale(LC_ALL, "RUS");

	sizeVArray = MAXSIZE;
	positionInMatrix = 0;
	VArray = new Vtype[sizeVArray];
}


template < class Vtype >
TVector<Vtype>::TVector(const int _sizeVArray) {
	sizeVArray = _sizeVArray;
	positionInMatrix = 0;
	VArray = new Vtype[sizeVArray];
}
template < class Vtype >
TVector<Vtype>::TVector(const int _sizeVArray,const int _positionInMatrix, const Vtype value) {
	setlocale(LC_ALL, "RUS");

	sizeVArray = _sizeVArray;
	positionInMatrix = _positionInMatrix;
	VArray = new Vtype[sizeVArray];
	if (VArray != nullptr) {
		for (int i = 0; i < sizeVArray; i++)
			VArray[i] = value;
	}
	else throw std::exception("��������� ������ �� ���������.");
}
template < class Vtype >
TVector<Vtype>::TVector(const int _sizeVArray,const int _positionInMatrix, const Vtype* _VArray) {
	setlocale(LC_ALL, "RUS");

	sizeVArray = _sizeVArray;
	positionInMatrix = _positionInMatrix;
	VArray = new Vtype[sizeVArray];
	if (VArray != nullptr) {
		for (int i = 0; i < sizeVArray; i++)
			VArray[i] = _VArray[i];
	}
	else throw std::exception("��������� ������ �� ���������.");
}
template < class Vtype >
TVector<Vtype>::TVector(const TVector<Vtype>& vector) {
	setlocale(LC_ALL, "RUS");

	sizeVArray = vector.sizeVArray;
	positionInMatrix = vector.positionInMatrix;
	VArray = new Vtype[sizeVArray];
	if (VArray != nullptr) {
		for (int i = 0; i < sizeVArray; i++)
			VArray[i] = vector.VArray[i];
	}
	else throw std::exception("��������� ������ �� ���������.");
}


template < class Vtype >
TVector<Vtype>::~TVector() {
	if (sizeVArray != NULL) {
		delete[] VArray;
		VArray = nullptr;
		sizeVArray = NULL;
		positionInMatrix = -1;
	}
}


template < class Vtype >
int TVector<Vtype>::getSizeVArray() const {
	return sizeVArray;
}
template < class Vtype >
int TVector<Vtype>::getPositionInMatrix() const {
	return positionInMatrix;
}


template < class Vtype >
Vtype TVector<Vtype>::vectorLenght() const {
	Vtype result = 0;
	for (int i = 0; i < sizeVArray; i++)
		result += VArray[i] * VArray[i];
	return sqrt(result);
}


template < class Vtype >
TVector<Vtype> TVector<Vtype>::operator+(const Vtype value) {
	TVector<Vtype> tmp(*this);
	for (int i = 0; i < sizeVArray; i++) {
		tmp.VArray[i] = VArray[i] + value;
	}
	return tmp;
}
template < class Vtype >
TVector<Vtype> TVector<Vtype>::operator-(const Vtype value) {
	TVector<Vtype> tmp(*this);
	for (int i = 0; i < sizeVArray; i++) {
		tmp.VArray[i] = VArray[i] - value;
	}
	return tmp;
}


template < class Vtype >
TVector<Vtype> TVector<Vtype>::operator + (const TVector<Vtype>& vector) {
	setlocale(LC_ALL, "RUS");

	TVector<Vtype> tmp(*this);
	if (sizeVArray == vector.sizeVArray) {
		tmp.sizeVArray = sizeVArray;
		for (int i = 0; i < sizeVArray; i++)
			tmp.VArray[i] = VArray[i] + vector.VArray[i];
		return tmp;
	}
	else throw std::exception("���������� ��������� � ���� �������� �� ���������.");
	return tmp;
}
template < typename Vtype >
TVector<Vtype> TVector<Vtype>::operator - (const TVector<Vtype>& vector) {
	setlocale(LC_ALL, "RUS");

	TVector<Vtype> tmp(*this);
	if (sizeVArray == vector.sizeVArray) {
		tmp.sizeVArray = sizeVArray;
		for (int i = 0; i < sizeVArray; i++)
			tmp.VArray[i] = VArray[i] - vector.VArray[i];
		return tmp;
	}
	else throw std::exception("���������� ��������� � ���� �������� �� ���������.");
	return *this;
}


template < class Vtype >
TVector<Vtype> TVector<Vtype>::operator * (const Vtype value) {
	TVector<Vtype> tmp(*this);
	for (int i = 0; i < sizeVArray; i++)
		tmp[i] = value * VArray[i];
	return tmp;
}
template < class Vtype >
Vtype TVector<Vtype>::operator * (const TVector<Vtype>& vector) {
	setlocale(LC_ALL, "RUS");

	Vtype result = 0;

	if (sizeVArray == vector.sizeVArray) {
		for (int i = 0; i < sizeVArray; i++)
			result += VArray[i] * vector.VArray[i];
	}
	else throw std::exception("���������� ��������� � ���� �������� �� ���������.");
	return result;
}


template < typename Vtype >
TVector<Vtype>& TVector<Vtype>::operator = (const TVector<Vtype>& vector) {
	setlocale(LC_ALL, "RUS");

	if (this != &vector) {
		if (sizeVArray != vector.sizeVArray) {
			delete[] VArray;
			sizeVArray = vector.sizeVArray;
			positionInMatrix = vector.positionInMatrix;
			VArray = new Vtype[sizeVArray];
		}
		if (VArray != nullptr) {
			positionInMatrix = vector.positionInMatrix;
			for (int i = 0; i < sizeVArray; i++) {
				VArray[i] = vector.VArray[i];
			}
		}
		else throw std::exception("��������� ������ �� ���������.");
	}
	return *this;
}


template < typename Vtype >
const bool TVector<Vtype>::operator == (const TVector<Vtype>& vector) {
	if (sizeVArray == vector.sizeVArray) {
		for (int i = 0; i < sizeVArray; i++) {
			if (VArray[i] != vector.VArray[i]) return false;
			return true;
		}
	}
	return false;
}
template < typename Vtype >
const bool TVector<Vtype>::operator != (const TVector<Vtype>& vector) {
	return !(*this == vector);
}


template < typename Vtype >
Vtype& TVector<Vtype>::operator[](const int index) {
	setlocale(LC_ALL, "RUS");

	if (index < 0 || index >= sizeVArray) {
		throw std::exception("�������� ����� �������� � �������.");
		return VArray[0];
	}
	else return VArray[index];
}
template < typename Vtype >
const Vtype& TVector<Vtype>::operator[](const int index) const {
	setlocale(LC_ALL, "RUS");

	if (index < 0 || index >= sizeVArray) {
		throw std::exception("�������� ����� �������� � �������.");
		return VArray[0];
	}
	else return VArray[index];
}

